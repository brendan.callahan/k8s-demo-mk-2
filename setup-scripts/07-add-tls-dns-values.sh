#!/bin/bash
set -euo pipefail
export $(cat .env | xargs)

for deploy in "cert-manager" "cert-manager-cainjector" "cert-manager-webhook"; \
  do kubectl -n cert-manager rollout status deploy/${deploy}; \
done

failcheck="${GITLAB_REPO_URL}failcheck"
failcheck="${DIGITALOCEAN_PAT_CERT_CHALLENGE}failcheck"
failcheck="${LETSENCRYPT_EMAIL}failcheck"

echo -n $DIGITALOCEAN_PAT_CERT_CHALLENGE | kubectl create secret generic dnspat -n cert-manager --dry-run=client --from-file=pat=/dev/stdin -o json > do_pat_certmanager.json

kubeseal --format=yaml < do_pat_certmanager.json > sealed-pat-certmanager.yaml

cat << EOF > cert-issuers.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: cert-issuers
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: k8s-core/issuers
  destination:
    server: https://kubernetes.default.svc
    namespace: cert-manager
  syncPolicy:
    automated:
      prune: true
      selfHeal: true  
EOF

mkdir k8s-core/issuers
mv cert-issuers.yaml k8s-core
mv sealed-pat-certmanager.yaml k8s-core/issuers

cat << EOF > cluster-issuer.yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging
  namespace: cert-manager
spec:
  acme:
    # The ACME server URL
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: $LETSENCRYPT_EMAIL
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt-staging
    # Enable the DNS-01 challenge provider
    solvers:
    # An empty 'selector' means that this solver matches all domains
    - selector: {}
      dns01:
        digitalocean:
          tokenSecretRef:
            name: dnspat
            key: pat
EOF

mv cluster-issuer.yaml k8s-core/issuers
rm do_pat_certmanager.json

git add k8s-core
git commit -m "add letsencrypt cluster issuer"
git push origin master

argocd app sync k8s-core-apps
