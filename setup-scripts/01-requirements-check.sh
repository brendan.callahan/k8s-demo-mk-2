#!/bin/bash

set -euo pipefail
kubectl version
kubeseal --version
argocd version --client
step version
