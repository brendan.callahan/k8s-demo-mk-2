#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"

cat << EOF > auth.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: auth
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: auth
  destination:
    server: https://kubernetes.default.svc
    namespace: argocd
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
EOF

mv auth.yaml apps

mkdir auth

cat << EOF > sso.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: sso
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: auth/sso
  destination:
    server: https://kubernetes.default.svc
    namespace: keycloak
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
EOF

mv sso.yaml auth

cat << EOF > keycloak-postgres.yaml
apiVersion: "acid.zalan.do/v1"
kind: postgresql
metadata:
  name: sso-postgres
spec:
  teamId: sso
  volume:
    size: 8Gi
  numberOfInstances: 1
  users:
    admin:
    - superuser
    - createdb
    keycloak: []
  databases:
    root: keycloak
  enableLogicalBackup: false
  postgresql:
    version: "13"
  enableShmVolume: true 
EOF

mkdir auth/sso
mv keycloak-postgres.yaml auth/sso

git add apps
git add auth
git commit -m "add keycloak app and database"
git push origin master

argocd app sync root
