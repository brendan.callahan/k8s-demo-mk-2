# K8s Demo Mk 2

Setup of K8s cluster with the following:

- ArgoCD
- linkerd2
- linkerd2-cni
- sealed-secrets
- externalDNS
- cert-manager
- Keycloak
- OAuth2-Proxy
- Falco

### Pre-requisites:

- Fork this repo
- Delete everything other than setup scripts, the `example.env` file and the `.gitignore file`
- Commit and push the branch after these deletes
- Use the `example.env` file to create your applicable `.env` file. Note that comments (# ...) are not valid in this file, so please remove them.
- A running kubernetes cluster (from terraform or manually)
  - See [Terraform repo](https://gitlab.com/abhagat-personal/terraform-kubernetes-demo) for example setup with Gitlab pipelines
- Access to a domain name (or obtain a new one for the test) 
- A DNS zone + domain name on digitalocean (for now -this is Linode compatible with small fixes) (from terraform or manually)
  - See [How To Point to DigitalOcean Nameservers From Common Domain Registrars](https://www.digitalocean.com/community/tutorials/how-to-point-to-digitalocean-nameservers-from-common-domain-registrars)
- `kubectl` [installed](https://kubernetes.io/docs/tasks/tools/)
- A `kubeconfig` file located at `~/.kube/config` (`kubectl` instructions in this repo are using the default). You can get this from the DigitalOcean console.
- `argocd` [CLI tool installed](https://argoproj.github.io/argo-cd/cli_installation/)
- `sealed-secrets` [CLI tool installed](https://github.com/bitnami-labs/sealed-secrets#homebrew)
- The `step` [CLI tool installed](https://smallstep.com/docs/step-ca/installation)
- A DigitalOcean personal access token (create this in the console)
- A gitlab personal access token with read repository access

### Steps:
- `chmod +x` the contents of `setup-scripts`
- Run in numerical order
- Until you develop your own intuition on what "ready" looks like, wait about 3-5 mins between scripts. 

The deploy tool used here - ArgoCD - will be able to be accessed at `localhost:8080` after script #2. This tool will poll the gitlab repository to look for changes at something like every three minutes - so if you'd like things to happen a bit faster, use the UI to manually sync the specific projects you're bootstrapping. For convenience, the provided scripts sometimes will end with `argo app sync <app_name>` to try to do this for you.

In some cases, the port forwarding will not completely work on its own after step 3, in those cases, open a new terminal and run the port forwarding command manually per:
`kubectl port-forward svc/argocd-server -n argocd 8080:443` at this stage.

```
./setup-scripts/01-requirements-check.sh
./setup-scripts/02-argocd-setup.sh
./setup-scripts/03-argocd-login-and-root-app.sh
# etc
```
- For a couple manual steps in this sequence, you'll need to specify an argument or two to the scripts. There are only a couple cases and usually involve just manual copy/paste+formatting or Keycloak administration:

```
./setup-scripts/14-argocd-with-oidc.sh argocd-oidc-client-secret-i-created-in-keycloak
```

#### Additional documentation
See [Keycloak - Argo CD](https://argoproj.github.io/argo-cd/operator-manual/user-management/keycloak/) for some helpful instructions on setting up ArgoCD access via Keycloak

### Important Reminders: 

- This will incur both fixed-cost and variable-cost charges. The fixed costs are any pool workers you've stood up for the k8s cluster. Installing the ingress controller will also create a (S) class DigitalOcean Load Balancer, which accrues at $10/month at time of writing. Adding Postgres volumes will incur a fixed cost charge of $0.10/GB/month - the volume is currently set to 8 GB.
- When you clean up your Kubernetes cluster, be sure to:
- [ ] delete the created Load Balancer ("Networking")
- [ ] delete any Persistent Volume Claims related to this project ("Volumes")
- [ ] delete any DNS A and TXT records created by this project ("Networking")
